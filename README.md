# Easy OpenVPN

Install easily a basic OpenVPN under docker

## COMMANDS

### install Quiclky
```shell script
$ make init SERVER_NAME=vpn.mondomaine.com SERVER_PORT=1194
```

### Generate configuration files :   
Generate new configuration for udp://$SERVER_NAME:$SERVER_PORT  
ex: 
```shell script
$ make genconfig SERVER_NAME=vpn.mondomaine.com SERVER_PORT=1194
```


### Get a new client certficate
Generate new client certificate using variable CLIENT_NAME
ex: 
```shell script
$ make new_certif CLIENT_NAME=MisterSmith
```

### Start Service
start:                         Start VPN service

### Stop service
stop:                          Stop VPN service
