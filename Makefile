ifndef VERBOSE
.SILENT: .env
endif

DC:=@docker-compose
CC:=@docker

.PHONY: start stop restart


.env:
	@echo "SERVER_PORT=${SERVER_PORT}" > .env
init: ## start and init vpn  ex: make init  SERVER_NAME=vpn.mondomaine.com SERVER_PORT=1194
init: start genconfig initpki

initpki:
	docker-compose exec -T openvpn ovpn_initpki;

genconfig: ## Generate new configuration for udp://$SERVER_NAME:$SERVER_PORT  ex: make genconfig SERVER_NAME=vpn.mondomaine.com SERVER_PORT=1194
genconfig:
	docker-compose exec -T openvpn ovpn_genconfig -u udp://${SERVER_NAME}:${SERVER_PORT}

new_certif: ## Create new certificate for CLIENT_NAME
new_certif:
	docker-compose exec -T openvpn easyrsa build-client-full ${CLIENT_NAME} nopass

start: ## Start VPN service
start: .env
	$(DC) up -d

stop: ## Stop VPN service
stop:
	$(DC) stop

.SILENT:banner pre-commit
.DEFAULT_GOAL := help
banner:
	printf "\n"
	printf "\033[32m   ██████╗ ██████╗ ███████╗███╗   ██╗██╗   ██╗██████╗ ███╗   ██╗ \033[0m\n"
	printf "\033[32m  ██╔═══██╗██╔══██╗██╔════╝████╗  ██║██║   ██║██╔══██╗████╗  ██║\033[0m\n"
	printf "\033[32m  ██║   ██║██████╔╝█████╗  ██╔██╗ ██║██║   ██║██████╔╝██╔██╗ ██║\033[0m\n"
	printf "\033[32m  ██║   ██║██╔═══╝ ██╔══╝  ██║╚██╗██║╚██╗ ██╔╝██╔═══╝ ██║╚██╗██║\033[0m\n"
	printf "\033[32m  ╚██████╔╝██║     ███████╗██║ ╚████║ ╚████╔╝ ██║     ██║ ╚████║\033[0m\n"
	printf "\033[32m   ╚═════╝ ╚═╝     ╚══════╝╚═╝  ╚═══╝  ╚═══╝  ╚═╝     ╚═╝  ╚═══╝\033[0m\n"


##
help:banner

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?## .*$$)|(^## )' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/' | sed 's/Makefile.\(\s\)*//'
.PHONY: help
